
<?php 
include 'config.php';
include 'auth.php';
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Perpustakaan | Explore</title>
        <link href="admin/css/font-face.css" rel="stylesheet" media="all">
        <link href="admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,400;0,500;0,600;0,700;0,800;1,100;1,500;1,600;1,700&display=swap" rel="stylesheet">
        <link href="admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
        <link href="admin/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">
        <!-- Main CSS-->
        <link href="admin/css/theme.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <body class="animsition">
        <header class="explorebook">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap2">
                        <div class="logo d-block d-lg-none">
                        </div>
                        <div class="header-button2">
                            <div class="header-button-item has-noti js-item-menu">
                                <i class="zmdi zmdi-notifications"></i>
                                <div class="notifi-dropdown js-dropdown">
                                    <div class="notifi__title">
                                        <p>You have 3 Notifications</p>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c1 img-cir img-40">
                                            <i class="zmdi zmdi-email-open"></i>
                                        </div>
                                        <div class="content">
                                            <p>You got a email notification</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c2 img-cir img-40">
                                            <i class="zmdi zmdi-account-box"></i>
                                        </div>
                                        <div class="content">
                                            <p>Your account has been blocked</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c3 img-cir img-40">
                                            <i class="zmdi zmdi-file-text"></i>
                                        </div>
                                        <div class="content">
                                            <p>You got a new file</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__footer">
                                        <a href="#">All notifications</a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-button-item mr-0 js-sidebar-btn">
                                <i class="fas fa-cog"></i>
                            </div>
                            <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                        <i class="fas fa-users"></i>Account Setting</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="logout.php">
                                        <i class="fas fa-sign-out-alt"></i>Logout</a>
                                    </div>
                                   
                                 
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <style type="text/css">
            .category{
            width: 50%;
            }
        </style>
        <form method="post">
        <section class="item-grid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="images/explore.png">
                    </div>
                    <div class="col-md-6">
                        <div class="welcome-content">
                            <div class="welcome-content-caption pb-2">
                                Selamat Datang di Explore,
                            </div>
                            <div class="welcome-content-caption2 pb-1">
                                Rekomendasi buku diperbaharui setiap minggu! Jika anda ingin kembali ke dashboard tombol ini.
                            </div>
                            <a href="paneluser.php" class="btno-rounded-merah pt-2 pb-1"><i class="fas fa-search"></i> Kembali</a>
                            <br><br>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br>
                        <div class="tips" style="background-color: #ecf6ff;"><i class="fas fa-lightbulb" style="padding: 0.7em;color:#ffd75f; border-left-style: solid;border-left-width: 2px;border-left-color: #ffd75f"></i>Tips : Tekan icon "<i class="fa fa-copy"></i>" untuk menambahkan buku ke wishlist</div>
                        <!--  <div class="tips text-danger" style="background-color: #ecf6ff;"><i class="fas fa-exclamation-circle" style="padding: 0.7em;color:red; border-left-style: solid;border-left-width: 2px;border-left-color:red"></i>Masa Peminjaman Salah satu Buku anda akan berakhir!</div> -->
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-2 col-sm-6 col-xs-6 category">
                        <div class="book-category">
                            <div class="book-category-inner">
                                <img src="https://www.hachettebookgroup.com/wp-content/uploads/2017/07/sci-fi_fantasy_v1a-2.png?resize=768%2C420">
                                <div class="book-category-title">
                                    <div class="book-category-title-inner">
                                        Adventure
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 category">
                        <div class="book-category">
                            <div class="book-category-inner">
                                <img src="https://img.freepik.com/free-photo/3d-render-handgun-grunge-background-with-blood-splatters_1048-7708.jpg?size=626&ext=jpg">
                                <div class="book-category-title">
                                    <div class="book-category-title-inner">
                                        Thriller
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 category">
                        <div class="book-category">
                            <div class="book-category-inner">
                                <img src="https://images.squarespace-cdn.com/content/v1/5a4a8093d7bdce7948030b31/1549938371559-HVAQ6ZHN781ZYWPHGZW9/ke17ZwdGBToddI8pDm48kDZOCC1hxjAuvg_TbOcQw1J7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UW86c52r3DWAZNCNukv02ZfpyO7Jbahd_CnxJP7g2nwAXMResvPS5yaXh0S3y3Hzow/proletariat-fallback-bg.jpg?format=2500w">
                                <div class="book-category-title">
                                    <div class="book-category-title-inner">
                                        Fantasy
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 category">
                        <div class="book-category">
                            <div class="book-category-inner">
                                <img src="https://static.vecteezy.com/system/resources/previews/000/123/348/non_2x/halloween-zombie-hand-vector.jpg">
                                <div class="book-category-title">
                                    <div class="book-category-title-inner">
                                        Horror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 category">
                        <div class="book-category">
                            <div class="book-category-inner">
                                <img src="https://galaxypress.com/wp-content/uploads/2018/12/Best-Mystery-Books.jpg">
                                <div class="book-category-title">
                                    <div class="book-category-title-inner">
                                        Mystery
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <br>
            <style type="text/css">
                .explore-img{
                width: 100%;
                height:45vh;
                border-radius:5px;
                }
            </style>

            <div class="row">
                            <?php 
                
                $cari_buku = mysqli_query($config,"SELECT * FROM tbl_buku");
               
                
                while($f = mysqli_fetch_array($cari_buku)) {
             ?>
                <div class="col-sm-6 col-md-3 col-xs-6 mt-3">
        

                    <div class="explore-card" id="section">
                        <div class="p-3 position-absolute" style="background-color: rgba(0,0,0,0.55);color: #fff;border-radius: 6px">
                            <?php echo $f['likes'] ?> <i class="fas fa-heart text-danger"></i>
                        </div>
                        <!-- <div class="explore-item-img"> -->
                        <img class="explore-img" src="<?php echo $f['book_picture'] ?>">
                        <!--  </div> -->
                        <div class="tengah">
                            <div class="explore-title">
                               <?php echo $f['book_title']; ?>  
                            </div>
                        </div>
                        <span>
                            <div class="pl-4">
                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
                                <span> <i class="fas fa-star"></i></span>
                                <span> 4/5</span>
                            </div>
                        </span>
                        <div class="row">
                            <div class="feature-card-footer">
                                <a href="view_book.php?id=<?php echo $f['id'] ?>" class="be">Lihat</a>
                            </div>
                            <?php 
                            $idb = $f['id'];
                            $si = $_SESSION['id'];
                             $cek_archived = mysqli_query($config,"SELECT * FROM tbl_archived WHERE id_buku ='$idb' AND id_register='$si'");
                             $fa = mysqli_fetch_assoc($cek_archived);
                            if (isset($fa['id_register']) != 1) { ?>
                                   
                            <div class="archive-container position-absolute">
                                <a href="archive.php?id=<?php echo $f['id'] ?>" style="color:#fff;"><i class="fa fa-copy" id="archive-container"></i></a>
                            </div>
                            <?php }else{ ?>
                            <div class="archive-container position-absolute">
                                <a href="unarchive.php?id=<?php echo $f['id'] ?>" style="color:#007bff;"><i class="fas fa-archive" id="archive-container"></i></a>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                  <?php } ?>
            </div>
      
        </section>
        </form>
        <script type="text/javascript">
            var klik = document.querySelectorAll("#archive-container");
            klik.forEach(function(tmbl) {
                tmbl.onclick = function() {  
                    var x = tmbl.getAttribute("class");
                    if (x == "fa fa-copy") {
                        tmbl.className = "fas fa-archive";
                        tmbl.style.color = "#007bff";
                    }else{
                        tmbl.className = "fa fa-copy";
                        tmbl.style.color = "#999";
                    }
                    
                    
            
                };
            })
            
            
            
        </script>
        <!-- Jquery JS-->
        <script src="admin/vendor/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="admin/vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="admin/vendor/slick/slick.min.js"></script>
        <script src="admin/vendor/wow/wow.min.js"></script>
        <script src="admin/vendor/animsition/animsition.min.js"></script>
        <!-- Main JS-->
        <script src="admin/js/main.js"></script>
    </body>
</html>
<!-- end document-->