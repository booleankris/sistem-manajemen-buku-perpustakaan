<!DOCTYPE html>

<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <title>Perpustakaan | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" type="text/css" href="gabriel.css" />
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    </head>
    <body>
        <style type="text/css">
          body{
            background: linear-gradient(90deg, #0099ff, #5345ff);
          }
            .loginform-card {
                margin-left: 20%;
            }
            .loginform-container{
              padding: 2em;
              background-color: #fff;
              border-radius: 20px;
            }
        </style>
        <?php 
    include 'config.php';
    session_start();
      if (isset($_POST['kirim'])) {
       $u=$_POST['username'];
       $p=md5($_POST['password']);
       $cari=mysqli_query($config,"SELECT * from tbl_register where username='$u' AND password='$p' AND status ='2'");
       $data=mysqli_num_rows($cari);
       $ambil_data = mysqli_fetch_assoc($cari);   
       if ($data>0) { $_SESSION['status'] = "login"; $_SESSION['nama'] = $u; $_SESSION['id'] = $ambil_data['id']; header("location:paneluser.php"); }else{ echo "Login Gagal"; } } ?>
        <section class="loginform">
            <div class="container">
                <div class="loginform-container">
                  <div class="row">
                      <div class="c6">
                          <p style="text-align: center; font-family: 'Quicksand'; font-size: 23px;">Belum Punya Akun? Bergabung bersama kami sekarang!<a href="register.php"> Daftar disini</a></p>

                          <img src="images/login.png" width="100%" style="padding-top: 1.5em;" />
                      </div>
                      <div class="c6">
                          <div class="loginform-card">
                              <div class="loginform-caption">
                                  <div class="loginform-title">Login</div>
                                  <div class="loginform-line"></div>
                                  <br />
                                  <div class="loginform-desc" style="font-family: 'Quicksand';">
                                      <form method="post">
                                          <div class="form-input">
                                              <div class="username">
                                                  <p>Username</p>
                                                  <input type="text" name="username" class="ga-input-login" /><br />
                                              </div>
                                              <br />
                                              <div class="password">
                                                  <p>Password</p>
                                                  <input type="password" name="password" class="ga-input-login" /><br />
                                              </div>
                                              <br />

                                              <input type="submit" value="Login" name="kirim" class="btn classy-blue" />
                                              <div style="padding-top: 0.8em;">
                                                <input type="submit" value="Sign Up" name="kirim" class="btn classic-red" />
                                              </div>
                                          </div>
                                      </form>
                                      <br />
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
            <br />
        </section>
    </body>
</html>
