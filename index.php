  <!DOCTYPE html>

  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <title>Perpustakaan | Landing Page</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="style.css">
      <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    </head>
    <body>
    <section class="navbar">
      <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
          <i class="fas fa-bars"></i>
        </label>
        <label class="logo"><i class="fa fa-book-reader"></i> Perpustakaan</label>
        <ul>
          <li><a class="active" href="login.php">i<i class="fa fa-sign-in-alt"></i> Masuk</a></li>

        </ul>
      </nav>
    </section><br><br>
    <section class="hero-section">

      <div class="hero">
          <div class="overlay"></div>
          <div class="hero-caption">
            <i class="fa fa-book-reader" style="font-size:3em"></i><br><br>
            <div class="hero-first-caption">
              Selamat Datang di
            </div>
            <div class="hero-second-caption">
              Sistem Management Buku Perpustakaan
            </div>
          </div>
      </div>
    </section>
    <section class="about">
    <div class="container">
      <div class="row">
        <div class="c5">

          <div class="about-image"></div>

        </div>
    

        <div class="c7">
          <div class="about-caption">
            <div class="about-title">Tentang Kami</div>
            <div class="about-line"></div><br>
            <div class="about-desc" style="text-align: center;font-family: 'Quicksand';">
              Web Aplikasi Perpustakaan Yang Memudahkan Anda dalam Meminjam dan Mengembalikan Buku,Terutama pada saat pandemi ini.Aplikasi ini mempunyai fitur yang sangat memudahkan anda.
            </div>
            <br>
            
          </div>
        </div>
      </div>
    </div><br>
    </section>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
  <path fill="#0099ff" fill-opacity="1" d="M0,192L40,208C80,224,160,256,240,240C320,224,400,160,480,160C560,160,640,224,720,256C800,288,880,288,960,256C1040,224,1120,160,1200,160C1280,160,1360,224,1400,256L1440,288L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"></path>
</svg>
    <section class="feature">
      <div class="feature-header">
        <div class="feature-header-title">Fitur Kami</div>
        <div class="feature-header-line"></div>
      </div>
        <div class="container">
          <div class="row">
            <div class="c3">
              <div class="feature-item">
                <div class="feature-icon"><i class="fas fa-laptop-house"></i></div>
                  <div class="feature-title">
                    Pinjam Buku Online
                  </div>
                  <p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>  
            </div>
            <div class="c3">
              <div class="feature-item">
                <div class="feature-icon"><i class="fab fa-asymmetrik"></i></div>
                  <div class="feature-title">
                    Pelayanan Cepat
                  </div>
                  <p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>      
            </div>
            <div class="c3">
              <div class="feature-item">
                <div class="feature-icon"><i class="fas fa-tasks"></i></div>
                  <div class="feature-title">
                    Management Buku
                  </div>
                  <p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>      
            </div>
            <div class="c3">
              <div class="feature-item">
                <div class="feature-icon"><i class="fa fa-users"></i></div>
                  <div class="feature-title">
                    Management User
                  </div>
                  <p class="feature-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>      
            </div>
          </div><br><br>
    </section>
    <section class="u-level">
    <div class="container">
      <div class="row">
        <div class="c7">
         
          <div class="u-level-caption">
            <div class="u-level-title">Ayo Gabung Sekarang!</div>
            <div class="u-level-line"></div><br>
             <div class="vct-join" style="text-align: center;">
            <img width="55%" src="images/login.png">
          </div>
            <div class="u-level-desc" style="text-align: center;font-family: 'Quicksand';">
             Gabung Sekarang dan dapatkan pelayanan dari kami,Semakin sering meminjam anda akan mendapatkan status <i>Premium Member</i>!
            </div>
            <br><br>
            <div class="container">
              <div class="row">
<!--                 <div class="c3" style="text-align: center;">
                    <i class="fa fa-user" style="font-size: 5em;text-align: center;color:#3f51b5"></i>
                    <div class="user-level">
                      Member
                    </div>
                  </div>
                <div class="c3" style="text-align: center;">
                    <i class="fa fa-user-shield" style="font-size: 5em;text-align: center;color:#3f51b5"></i>
                    <div class="user-level">
                      Member Premium
                    </div>
                </div> -->
                <div class="c6">
                 
                </div>
              </div>
            </div>
            <br>

          </div>
        </div>
        <div class="c5">

          <div class="u-level-image"></div>

        </div>
      </div>
    </div><br>
    </section>
    </body>
  </html>
