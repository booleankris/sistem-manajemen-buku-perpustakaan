
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" 
    type="image/png" 
    href="https://new.myevents.id/img/128x128.png">
    <title>Guest Registration</title>
    <link rel="stylesheet" type="text/css" href="https://new.myevents.id/css/app1.css">
    <link href="https://new.myevents.id/frontend/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://new.myevents.id/css/register_guest.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.5/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
    <style type="text/css">
        html, body {
            background: url(https://new.myevents.id/img/bg.jpg);
        }
        @media  screen and (max-width: 767px) {
            .content {
                background: url(https://new.myevents.id/img/bg.jpg) !important;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 content">
                <div class="title">
                    <img alt="Icon" src="https://new.myevents.id/img/128x128.png" class="img-responsive" style="margin: 0 auto" />
                    <h1 class="text-center">Lomba Virtual GPN</h1>
                </div>
                <h4 class="text-center">Form Registrasi</h4>
                <hr>
                                <form action="/guest/register/35/step-one" method="post">
                    <input type="hidden" name="_token" value="3g398CTPgCaSd3uCaZAHHHDjAsDmvABwDYvaP57j">
                    <div class="form-group">
                        <label for="title">Nama Lengkap (*)</label>
                        <input type="text" value="" class="form-control" name="fullname" required placeholder="Masukan nama lengkap">
                    </div>
                     <div class="form-group">
                        <label for="description">Lomba Virtual</label>
                        <select name="lomba" class="form-control" required>
                            <option value="Lomba1" >--Pilih Lomba Virtual--</option>
                            <option value="Lomba2" >Lomba</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Kartu GPN</label>
                        <input type="file" id="input-file-now-custom-1" class="dropify" data-default-file="https://keuangansyariah.mysharing.co/htttp:/mysharing.co/wp-content/uploads//sites/3/2020/04/Ilustrasi-Debit-GPN-BNI-Syariah.jpeg" />

                    </div>
                   
                    <div class="form-group">
                        <label for="description">Nomor Handphone</label>
                        <input type="text" value="" class="form-control" name="fullname" required placeholder="Masukan Nomor Handphone Anda">
                    </div>
                   
                     <div class="form-group">
                        <label for="description">Alamat</label>
                        <input type="text" value="" class="form-control" name="fullname" required placeholder="Masukan Alamat Lengkap Anda">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-md btn-submit">Selanjutnya <i class="fa fa-arrow-circle-right"></i></button>
                </form>  
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.5/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $('.dropify').dropify();
        $(function() {
            $('.select2').select2();
            $('.numeric').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

            $(document).on('change', '#provinsi', function() {
                let id_provinsi = $(this).val();
                console.log(id_provinsi)
                $.get("https://new.myevents.id/get-cities/"+id_provinsi, function(data) {
                    let options = "";
                    $.each(JSON.parse(data), function(index, value) {
                        options += `<option value="${value.id}">${value.name}</option>`;
                    });
                    $('#kota').html(options);
                });
            });
        })
    </script>
</body>
</html>
