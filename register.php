<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Perpustakaan | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" type="text/css" href="gabriel.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </head>
  <body>
<?php 
    include 'config.php';
    if (isset($_POST['kirim'])) {
    $nama=$_POST['nama'];
    $kelamin=$_POST['jenis_kelamin'];
    $nim=$_POST['nim'];
    $fakultas=$_POST['fakultas'];
    $angkatan=$_POST['angkatan'];
    $email=$_POST['email'];
    $no_hp=$_POST['no_hp'];
    $alamat=$_POST['alamat'];
    $username=$_POST['username'];
    $password= md5($_POST['password']);
    $simpan_data=mysqli_query($config,"INSERT INTO tbl_register values('','$nama','$nim','$kelamin','$fakultas','$angkatan','$no_hp','$alamat','$username','$password','$email','1','1')");
    if ($simpan_data) {
      echo'Anda Berhasil Membuat Akun';
    }
    else{
      echo'Gagal';
    }
  }
?>

    <div class="formregister-title">Pendaftaran</div>
          <div class="formregister-line"></div><br>
  <section class="registerform">
  <div class="container">
    <div class="row">
      <div class="c12">
        <div style="display: flex;justify-content: center;">
          
        </div>
        <br>
      </div>
      <div class="c6">
      <div class="loginform-card">
        <div class="loginform-caption">
        
          <div class="loginform-desc">
            <form method="post">
             <div class="form-input">
                <div class="form-input-item">
                  <p>Nama Lengkap</p>
                  <input type="text" required name="nama" placeholder="Masukkan Nama" class="ga-input"><br>
                </div>
                <br>
                <div class="form-input-item">
                  <p>NIM</p>
                  <input type="number" name="nim" required placeholder="Masukkan NIM" class="ga-input"><br>
                </div><br>
                <div class="form-input-item">
                  <p>Jenis Kelamin</p>
                  <select class="ga-input" name="jenis_kelamin" required="">
                    <option value="">-Pilih Jenis Kelamin-</option>
                    <option value="Laki-laki">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select><br>
                </div><br>
                <div class="form-input-item">
                  <p>Fakultas</p>
                    <select class="ga-input" required name="fakultas">
                      <option value="">-Pilih Fakultas-</option>
                      <option value="Fakultas Ilmu Komputer dan Teknologi Informasi">Fakultas Ilmu Komputer dan Teknologi Informasi</option>
                      <option value="Fakultas Teknik">Fakultas Teknik</option>
                      <option value="Fakultas Keguruan dan Ilmu Pendidikan Universitas">Fakultas Keguruan dan Ilmu Pendidikan Universitas</option>
                      <option value="Fakultas Matematika dan Ilmu Pengetahuan Alam">Fakultas Matematika dan Ilmu Pengetahuan Alam</option>
                      <option value="Fakultas Ilmu Sosial dan Ilmu Politik">Fakultas Ilmu Sosial dan Ilmu Politik</option>
                      <option value="Fakultas Ilmu Budaya">Fakultas Ilmu Budaya</option>
                      <option value="Fakultas Ekonomi dan Bisnis">Fakultas Ekonomi dan Bisnis</option>
                      <option value="Fakultas Kehutanan">Fakultas Kehutanan</option>
                      <option value="Fakultas Farmasi">Fakultas Farmasi</option>
                      <option value="Fakultas Kedokteran">Fakultas Kedokteran</option>
                  </select><br>
                </div><br>                
                <div class="form-input-item">
                  <p>Angkatan</p>
                  <input type="number" placeholder="Masukkan" require name="angkatan" class="ga-input"><br>
                </div><br>                
                
                
             </div>
          </div>
          <br>
         </div>
        </div>


      </div>
  

      <div class="c6">
         <div class="form-input-item">
                  <p>Nomor HP</p>
                  <input type="text" placeholder="Masukkan Nomor Hp" require name="no_hp" class="ga-input"><br>
                </div><br>                
                <div class="form-input-item">
                  <p>Alamat</p>
                  <input type="text" placeholder="Masukkan Alamat Lengkap" require name="alamat" class="ga-input"><br>
                </div><br>
                <div class="form-input-item">
                  <p>Username</p>
                  <input type="text" placeholder="Masukkan Username" require name="username" class="ga-input"><br>
                </div><br>
                <div class="form-input-item">
                  <p>Password</p>
                  <input type="password" placeholder="Masukkan Password" require name="password" class="ga-input"><br>
                </div><br>
                <div class="form-input-item">
                  <p>Email</p>
                  <input type="email" placeholder="Masukkan Email" require name="email" class="ga-input"><br>
                </div><br>
                <div class="row">
                  <div class="c4">
                    </a>
                    <div class="tombol-register">
                      <a href="login.php"><input type="button" value="Kembali" name="cancel" class="btn classic-red-register">
                    </div>
                    </a>
                  </div>
                  <div class="c4">
                    <div class="tombol-register">
                      <input type="submit" value="Kirim!" name="kirim" class="btn classic-green-register">
                    </div>
                      
                  </div>
                </div>
                

      
      </div>
    </div>
  </div><br>
          </form>
  </section>
  </body>
</html>
  