<!DOCTYPE html>
<html>
<head>
	<title>contoh form</title>
</head>
<body>
	<!-- 
	jenis value attribute type
		text
		number
		radio
	 -->
	<?php 
		if (isset($_POST['btn'])) { //jika tombol submit yang namenya "btn" di click

			//aksi mengambil data dari inputan dan disimpan dalam variable
			$nama = $_POST['nama']; // ngambil data dari inputan yg name nya "nama"
			$kelas = $_POST['kelas']; // ngambil data dari inputan yg name nya "kelas"
			$nim = $_POST['nim'];// ngambil data inputan yg name nya "kelas"
			// menampilkan
			echo "Nama : $nama <br>Kelas : $kelas<br>NIM : $nim <br>";

		}
	 ?>
	<form method="post">
		<label>Nama</label>
		<input type="text" name="nama"> <!-- inputan yg name nya "name" -->
		<label>Kelas</label>
		<input type="text" name="kelas"><!-- inputan yg name nya "kelas" -->
		<label>NIM</label>
		<input type="text" name="nim"><!-- inputan yg name nya "nim" -->
		<input type="submit" name="btn">
	</form>
	
</body>
</html>