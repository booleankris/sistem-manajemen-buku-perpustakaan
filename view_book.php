<?php 
include 'config.php';
include 'auth.php';
	if (isset($_GET['id'])) {
		$idb = $_GET['id'];
	}
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Perpustakaan | Explore</title>
        <link href="admin/css/font-face.css" rel="stylesheet" media="all">
        <link href="admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,400;0,500;0,600;0,700;0,800;1,100;1,500;1,600;1,700&display=swap" rel="stylesheet">
        <link href="admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
        <link href="admin/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">
        <!-- Main CSS-->
        <link href="admin/css/theme.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <body class="animsition" background="">
        <header class="explorebook">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap2">
                        <div class="logo d-block d-lg-none">
                        </div>
                        <div class="header-button2">
                            <div class="header-button-item has-noti js-item-menu">
                                <i class="zmdi zmdi-notifications"></i>
                                <div class="notifi-dropdown js-dropdown">
                                    <div class="notifi__title">
                                        <p>You have 3 Notifications</p>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c1 img-cir img-40">
                                            <i class="zmdi zmdi-email-open"></i>
                                        </div>
                                        <div class="content">
                                            <p>You got a email notification</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c2 img-cir img-40">
                                            <i class="zmdi zmdi-account-box"></i>
                                        </div>
                                        <div class="content">
                                            <p>Your account has been blocked</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__item">
                                        <div class="bg-c3 img-cir img-40">
                                            <i class="zmdi zmdi-file-text"></i>
                                        </div>
                                        <div class="content">
                                            <p>You got a new file</p>
                                            <span class="date">April 12, 2018 06:50</span>
                                        </div>
                                    </div>
                                    <div class="notifi__footer">
                                        <a href="#">All notifications</a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-button-item mr-0 js-sidebar-btn">
                                <i class="fas fa-cog"></i>
                            </div>
                            <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                        <i class="fas fa-users"></i>Account Setting</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="logout.php">
                                        <i class="fas fa-sign-out-alt"></i>Logout</a>
                                    </div>
                                   
                                 
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <style type="text/css">
            .category{
            width: 50%;
            }
            .panel{
            	height: 47vh!important;
            	width: 100%;
            	background-color: #3289e6;
            }
        </style>
        <form method="post">
       		<?php

       			$si = $_SESSION['id'];
       			$cari_buku = mysqli_query($config,"SELECT * FROM tbl_buku WHERE id ='$idb'");
            
            	$f = mysqli_fetch_array($cari_buku);
            	
       		 ?>
       		 <div class="item-grid">
       		 	<div class="container">
       		 		<div class="row">
       		 			<div class="col-md-4">
       		 				<img width="100%" class="explore-img mt-5" src="<?php echo $f['book_picture'] ?>">
       		 			</div>
       		 			<div class="col-md-8 mt-5 pt-4">
       		 				<h2><?php echo $f['book_title'] ?></h2>
       		 				 <span>
       		 				<div class="">
	                            <div class="pl-4 pt-3">
	                            	<span >Book Rating</span>
	                                <span> <i class="fas fa-star pl-2" style="color:#ffc107"></i></span>
	                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
	                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
	                                <span> <i class="fas fa-star" style="color:#ffc107"></i></span>
	                                <span> <i class="fas fa-star"></i></span>
	                                <span> 4/5</span>
	                                <span><a href="add_to_rencanapinjam.php?idb=<?php echo $f['id'] ?>&idr=<?php echo $si ?>"><input type="button" class="bp" value="Pinjam"></a></span>
	                            </div>
	                        </div>
	                        <?php 
	                        	$cari_like = mysqli_query($config,"SELECT * FROM tbl_likes WHERE id_buku ='$idb' AND id_register='$si'");
								$fa = mysqli_fetch_assoc($cari_like);

	                        	if (isset($fa['id_register']) != 1) { 
	                         ?>
	                        <div class="">
	                            <div class="pl-3 pt-2 pb-2">
	                            	<a href="like.php?id=<?php echo $f['id'] ?>"><i class="fas fa-heart" id="like-container" style="font-size: 1.5em;color: #999;"></i></a>
	                            	<span style="font-size: 1em;"><sup><?php echo $f['likes'] ?></sup></span>
	                            </div>
	                        </div>
	                    <?php }else{ ?>
	                    	<div class="">
	                            <div class="pl-3 pt-2 pb-2">
	                            	<a href="unlike.php?id=<?php echo $f['id'] ?>"><i class="fas fa-heart text-danger" id="like-container" style="font-size: 1.5em;"></i></a>
	                            	<span style="font-size: 1em;"><b><sup><?php echo " ".$f['likes'] ?><a class="pl-1">Liked!</a></sup> </b></span>
	                            </div>
	                        </div>
	                    <?php } ?>
	                    <span class="badge badge-pill badge-primary p-2 ml-1">Fantasy</span><span class="badge badge-pill badge-primary p-2 ml-1">Adventure</span>
	                        <div class="row">
	                        	<div class="col-md-6">
		                        	<div class="book-info-desc pt-3 pb-3">
			                        	<h5>Description :</h5> 
			                        </div>
			                        <?php echo $f['book_desc'] ?>
	                        	</div>
	                        	<div class="col-md-6">
		                        	<div class="book-info-desc pt-3 pb-3">
			                        	<h5>Synopsis :</h5>
	 	                        </div>
	 	                        <?php echo $f['book_synopsis'] ?>
	                        	</div>
	                        </div>
                        </span>
       		 			</div>
       		 		</div>
       		 	</div>
       		 </div>
        </form>
       <script type="text/javascript">
            var klik = document.querySelectorAll("#like-container");
            klik.forEach(function(tmbl) {
                tmbl.onclick = function() {  
                    var x = tmbl.getAttribute("class");
                    if (x == "fas fa-heart text-danger") {
                        tmbl.className = "fa fa-heart";
                        tmbl.style.color = "#dc3545";
                    }else{
                        tmbl.className = "fa fa-heart text-danger";
                        tmbl.style.color = "#999";
                    }
                    
                    
            
                };
            })
        </script>
        <!-- Jquery JS-->
        <script src="admin/vendor/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="admin/vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="admin/vendor/slick/slick.min.js"></script>
        <script src="admin/vendor/wow/wow.min.js"></script>
        <script src="admin/vendor/animsition/animsition.min.js"></script>
        <!-- Main JS-->
        <script src="admin/js/main.js"></script>
    </body>
</html>
<!-- end document-->
